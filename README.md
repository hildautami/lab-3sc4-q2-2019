# Git

Selain belajar membuat aplikasi android, di kelas ini kalian akan diberikan sebuah pengalaman baru dengan menggunakan Git.

##### Version Control System (VCS)

VCS merupakan sebuah software yang membantu para developer untuk saling bekerja sama dan tetap mempertahankan riwayat kerja mereka secara utuh.

VCS memiliki beberapa fungsi, di antaranya mengizinkan para developer untuk  bekerja secara bersamaan, tidak mengizinkan saling tumpang-tindih perubahaan yang ada, dan mempertahankan riwayat dari setiap versi.

VCS sendiri terbagi menjadi dua tipe, yaitu Centralized VCS (CVCS) dan Distributed/Decentralized VCS (DVCS).

Git merupakan VCS yang sering digunakan sekarang ini. Git merupakan salah satu Distributed Version Control System (DVCS), yang berarti salinan kode yang tersimpan di local repository adalah sebuah version control repository yang
lengkap.

Jadi, antara local repository dan remote repository sama-sama berisi versi terakhir (terupdate).

##### Perintah di Git
Untuk melihat berbagai perintah (command) yang ada di Git, kamu bisa melihatnya di sini.

##### Tutorial Git
Pada bagian pertama, kamu sudah diberikan soal tutorial mengenai bahasa pemrograman Java. Pada bagian ini, kamu akan menyimpan file yang telah kamu buat secara remote di GitLab. Untuk melakukan hal tersebut, harap ikuti langkah-langkah berikut ini:

1. Buatlah sebuah folder yang akan menjadi local repository yang kamu gunakan,. Nama folder bebas. 
*Catatan: dalam soal tutorial ini, nama folder yang digunakan adalah “TutorialLab”, kalian boleh menggantinya.

2. Buka command prompt/shell.

3. Ubah direktori sekarang menjadi direktori dari folder “TutoriaLab"
Misal : D:\Lab\TutorialLab

4. Jalankan perintah `git init` pada command prompt/shell.
(ini akan menginisiasi Git pada direkori yang ditunjuk oleh Command Prompt, pastikan kalian tidak salah menginisiasi folder)

5. Jalankan perintah `git status` untuk memastikan bahwa inisiasi repository Git sudah berhasil dilakukan.

6. Atur username dan email yang akan diasosiasikan dengan pekerjaanmu di repository Git dengan perintah berikut: `git config --global user.name “<nama-lengkapmu”
git config --global user.email “<masukkan-emailmu>”`
ini untuk memberi informasi tentang pengguna repo, masukan email dan nama kalian.

7. Setelah melakukan konfigurasi, Buatlah file `Halo.txt` di dalam folder “TutorialLab”.

8. Setelah itu, jalankan perintah `git status` pada command prompt/shell, maka nanti akan muncul daftar untracked files dari folder tersebut. Oleh karena itu, jalankan perintah `git add <nama-file>` untuk setiap untracked files.

9. Setelah itu, jalankan perintah `git commit -m “<isi-dengan-pesanmu>”`. Hal tersebut berfungsi sebagai penanda terhadap perubahan yang terjadi di dalam files yang kamu buat sebelumnya.

10. Dengan begitu, kamu sudah berhasil mengimplementasikan Git di local repository. Untuk tahapan berikutnya, kamu akan membagikan hasil kerjamu kepada orang lain melalui GitLab, yang terkoneksi dengan local repository yang kamu miliki. Buka GitLab melalui browser kalian.

11. Buat sebuah project baru, kamu bisa atur nama project sesukamu, dan atur Visibility Level menjadi Private, sehingga hasil pekerjaan kalian tidak bisa dimanfaatkan pihak yang tidak bertanggung jawab.
12. Silakan tekan tombol copy yang berada di sebelah baris yang berisi link menuju repository kamu.
13. Buka kembali command prompt/shell kamu, lalu ketikkan perintah:
First Tab:
    ```sh
    git remote add origin <CLONEURL>
    ```
    diganti dengan link yang sudah kamu copy sebelumnya. Contoh:
    ```sh
    git remote add origin https://gitlab.com/.../....git
    ```
